package com.devilwwj.menu;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    // 定义菜单项id
    private static final int ITEM1 = Menu.FIRST;
    private static final int ITEM2 = Menu.FIRST + 1;
    private static final int ITEM3 = Menu.FIRST + 2;
    private static final int ITEM4 = Menu.FIRST + 3;
    private static final int ITEM5 = Menu.FIRST + 4;
    private static final int ITEM6 = Menu.FIRST + 5;
    private static final int ITEM7 = Menu.FIRST + 6;
    private static final int ITEM8 = Menu.FIRST + 7;
    private static final int ITEM9 = Menu.FIRST + 8;

    private static final String TAG = MainActivity.class.getSimpleName();
    private Button contextMenuButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contextMenuButton = (Button) findViewById(R.id.button);
        registerForContextMenu(contextMenuButton);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // 采用硬编码的形式实现菜单
        // 直接设置标题
//        menu.add("菜单项1");
//        menu.add("菜单项2");
//        menu.add(1, ITEM1, 1, "菜单项1");
//        menu.add(1, ITEM2, 2, "菜单项2");
//        menu.add(2, ITEM3, 3, "菜单项3");
//        menu.add(2, ITEM4, 4, "菜单项4");
//
//
//        // 添加子菜单
//        SubMenu subMenu = menu.addSubMenu(1, ITEM5, 5, "子菜单1");
//        subMenu.add(1, ITEM7, 1, "子菜单项1");
//        subMenu.add(1, ITEM8, 2, "子菜单项2");
//        subMenu.add(1, ITEM9, 3, "子菜单项3");
//
//        menu.addSubMenu(1, ITEM6, 6, "子菜单2");


        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case ITEM1:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case ITEM2:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case ITEM3:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case ITEM4:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case ITEM5:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case ITEM6:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case ITEM7:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case ITEM8:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case ITEM9:
                Toast.makeText(this, "你点击了" + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
        }

        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        // set context menu title
        menu.setHeaderTitle("文件操作");
        // add context menu item
        menu.add(0, 1, Menu.NONE, "发送");
        menu.add(0, 2, Menu.NONE, "标记为重要");
        menu.add(0, 3, Menu.NONE, "重命名");
        menu.add(0, 4, Menu.NONE, "删除");
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // 得到当前被选中的item信息
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Log.v(TAG, "context item seleted ID=" + menuInfo.id);

        switch (item.getItemId()) {
            case 1:
                // do something
                break;
            case 2:
                // do something
                break;
            case 3:
                // do something
                break;
            case 4:
                // do something
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }


    public void showPopupMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu_main, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.mi1:
                        // do something
                        break;
                    case R.id.mi2:
                        // do something
                        break;
                    case R.id.mi3:
                        // do something
                        break;
                    case R.id.mi4:
                        // do something
                        break;
                }
                return false;
            }
        });

        popupMenu.show();

    }
}
