package com.devilwwj.androiddevelopcourse.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ExpandableListView;

import com.devilwwj.androiddevelopcourse.R;
import com.devilwwj.androiddevelopcourse.adapters.ExpandListViewAdapter;
import com.devilwwj.androiddevelopcourse.domain.Category;
import com.devilwwj.androiddevelopcourse.domain.GroupItem;
import com.devilwwj.androiddevelopcourse.utils.ResourceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A022-列表容器之ExpandableListView
 *
 * @author devilwwj
 */
public class ExpandableListViewTestActivity extends ActionBarActivity implements ExpandListViewAdapter.OnGroupClickListener, ExpandListViewAdapter.OnGroupExpandListener
        , ExpandListViewAdapter.OnChildItemClickListener {
    private ExpandableListView expandableListView;
    private Context mContext;
    private ExpandListViewAdapter expandAdapter;
    private List<GroupItem> groupList;
    private List<List<Category>> childList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expandablelistview);
        mContext = ExpandableListViewTestActivity.this;


        expandableListView = (ExpandableListView) this.findViewById(R.id.expandablelistview);
        expandableListView.setGroupIndicator(null); // 去掉默认指示器

        // 设置展开列表数据
        setExpandableListView();

    }


    private void setExpandableListView() {
        try {
            // 这里分别模拟组项和子项数据
            groupList = new ArrayList<GroupItem>();
            childList = new ArrayList<List<Category>>();

            ResourceUtil resourceUtil = new ResourceUtil(this);
            // 从本地获取目录
            String str = getString(R.string.categories);
            JSONObject jsonObject = new JSONObject(str);
            JSONObject categoryObj = jsonObject.optJSONObject("categories");
            // 左边侧边栏
            JSONArray jsonArray = categoryObj.optJSONArray("left");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONArray groupArray = jsonArray.getJSONArray(i);
                GroupItem groupItem = new GroupItem();

                groupItem.setDrawableId(resourceUtil.getResId("ic_leftnav_"
                        + (i + 1), "drawable"));
                JSONObject groupObj = groupArray.optJSONObject(0);
                groupItem.setGroupId(groupObj.optString("cat_id")); // 设置组别的分类id
                groupItem.setText(groupObj.optString("title"));
                groupList.add(groupItem);
                List<Category> categories = new ArrayList<Category>();
                if (groupArray.length() > 0) { // 如果多于一项分类
                    for (int index = 1; index < groupArray.length(); index++) {
                        JSONObject itemObj = groupArray.optJSONObject(index);
                        Category categorie = new Category();
                        categorie.setTitle(itemObj.optString("title"));
                        categorie.setCat_id(itemObj.optString("cat_id"));
                        categories.add(categorie);
                    }
                }

                childList.add(categories);
            }

            // 实例化适配器
            expandAdapter = new ExpandListViewAdapter(this, expandableListView, groupList, childList);
            expandableListView.setAdapter(expandAdapter);


            // 设置ExpandableListView的相关事件监听
            // 子项选中、子项被点击、组项展开、组项被点击
            // expandableListView.setOnItemSelectedListener(itemSelectedListener);
            expandableListView.setOnChildClickListener(mOnChildClickListener);
//            expandableListView.setOnGroupExpandListener(mOnGroupExpandListener);
            // expandableListView.setOnGroupClickListener(mOnGroupClickListener);
            expandAdapter.setOnGroupClickListener(this);
            expandAdapter.setOnGroupExpandListener(this);
            expandAdapter.setOnChildClickListener(this);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    final private ExpandableListView.OnChildClickListener mOnChildClickListener = new ExpandableListView.OnChildClickListener() {

        @Override
        public boolean onChildClick(ExpandableListView parent, View v,
                                    final int groupPosition, final int childPosition, long id) {

            // 设置子项被选中的背景
            // expandAdapter.setItemChecked(groupPosition, childPosition);

            // 返回true，列表不可展开
            return false;
        }
    };

    final private ExpandableListView.OnGroupExpandListener mOnGroupExpandListener = new ExpandableListView.OnGroupExpandListener() {

        @Override
        public void onGroupExpand(int groupPosition) {
            for (int i = 0, count = expandableListView
                    .getExpandableListAdapter().getGroupCount(); i < count; i++) {
                expandAdapter.setExpandedGroupPosition(groupPosition);
                if (groupPosition != i) { // 关闭其他组
                    expandableListView.collapseGroup(i);
                }
            }
        }
    };

    final private ExpandableListView.OnGroupClickListener mOnGroupClickListener = new ExpandableListView.OnGroupClickListener() {

        @Override
        public boolean onGroupClick(ExpandableListView parent, View v,
                                    final int groupPosition, long id) {

            return true;
        }
    };

    @Override
    public void onGroupExpand(int position) {
        // 组项被展开时会回调这个方法
        if (expandAdapter.getExpandStateAtPosition(position)) {
            // 收起
            expandableListView.collapseGroup(position);
            expandAdapter.setExpandStateAtPosition(position, false);
        } else {
            expandableListView.expandGroup(position, true);
            expandAdapter.setExpandStateAtPosition(position, true);
        }
    }

    @Override
    public void onGroupClick(int position) {
        // 执行选中后的操作
    }

    @Override
    public void onChildItemClick(int groupPosition, int childPosition) {
        // 子项被点击会回调这个方法
    }
}
