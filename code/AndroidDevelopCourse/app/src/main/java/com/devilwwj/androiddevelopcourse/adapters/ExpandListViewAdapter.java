package com.devilwwj.androiddevelopcourse.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devilwwj.androiddevelopcourse.R;
import com.devilwwj.androiddevelopcourse.domain.Category;
import com.devilwwj.androiddevelopcourse.domain.GroupItem;

import java.util.HashMap;
import java.util.List;

/**
 * 自定义可展开列表的适配器
 */
public class ExpandListViewAdapter extends BaseExpandableListAdapter {

	private Context mContext;
	private ExpandableListView expandableListView;
	private List<GroupItem> groupList;
	private List<List<Category>> childList;
	private LayoutInflater inflater;

	private OnGroupExpandListener OnGroupExpandListener;
	private OnGroupClickListener onGroupClickListener;
	private OnChildItemClickListener onChildClickListener;
	private HashMap<Integer, Boolean> maps = new HashMap<Integer, Boolean>();

	private boolean expandStateAtPosition = false;

	public OnGroupClickListener getOnGroupClickListener() {
		return onGroupClickListener;
	}

	public void setOnGroupClickListener(
			OnGroupClickListener onGroupClickListener) {
		this.onGroupClickListener = onGroupClickListener;
	}

	public OnGroupExpandListener getOnGroupExpandListener() {
		return OnGroupExpandListener;
	}

	public void setOnGroupExpandListener(
			OnGroupExpandListener onGroupExpandListener) {
		OnGroupExpandListener = onGroupExpandListener;
	}

	public OnChildItemClickListener getOnChildClickListener() {
		return onChildClickListener;
	}

	public void setOnChildClickListener(
			OnChildItemClickListener onChildClickListener) {
		this.onChildClickListener = onChildClickListener;
	}

	private int mExpandedGroupPosition;

	public int getExpandedGroupPosition() {
		return mExpandedGroupPosition;
	}

	public void setExpandedGroupPosition(int mExpandedGroupPosition) {
		this.mExpandedGroupPosition = mExpandedGroupPosition;

	}

	public ExpandListViewAdapter(Context context,
								 ExpandableListView expandableListView, List<GroupItem> groupList,
								 List<List<Category>> childList) {
		super();
		this.mContext = context;
		this.expandableListView = expandableListView;
		this.groupList = groupList;
		this.childList = childList;

		inflater = LayoutInflater.from(context);

		// 初始化列表展开状态
		for (int i = 0; i < groupList.size(); i++) {
			maps.put(i, false);
		}
	}

	private int mGroupPosition = 0;
	private int mChildPosition = 0;

	public void setItemChecked(int groupPosition, int childPosition) {
		if (expandableListView == null) {
			return;
		}
		this.mGroupPosition = groupPosition;
		this.mChildPosition = childPosition;

		int numberOfGroupThatIsOpened = 0;

		for (int i = 0; i < groupPosition; i++) {
			if (expandableListView.isGroupExpanded(i)) {
				numberOfGroupThatIsOpened += this.getChildrenCount(i);
			}
		}

		int position = numberOfGroupThatIsOpened + groupPosition
				+ childPosition + 1;

		if (!expandableListView.isItemChecked(position)) {
			expandableListView.setItemChecked(position, true);
		}
	}

	@Override
	public int getGroupCount() {
		return groupList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return childList.get(groupPosition).size();
	}

	/*
	 * 获得组项 (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroup(int)
	 */
	@Override
	public GroupItem getGroup(int groupPosition) {
		return groupList.get(groupPosition);
	}

	@Override
	public Category getChild(int groupPosition, int childPosition) {
		return childList.get(groupPosition).get(childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		final GroupViewHolder groupViewHolder;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.layout_expand_group, parent,
					false);
			groupViewHolder = new GroupViewHolder(convertView);
			convertView.setTag(groupViewHolder);
		} else {
			groupViewHolder = (GroupViewHolder) convertView.getTag();
		}

		GroupItem groupItem = groupList.get(groupPosition);
		groupViewHolder.itemGroupIcon.setBackgroundResource(groupItem
				.getDrawableId());
		groupViewHolder.itemGroupText.setText(groupItem.getText());

		// 如果该组没有子项，则不显示箭头
		if (childList.get(groupPosition).size() == 0) {
			groupViewHolder.itemArrow.setVisibility(View.GONE);
			groupViewHolder.itemGroupLayout
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							onGroupClickListener.onGroupClick(groupPosition);
						}
					});
		} else {
			groupViewHolder.itemArrow.setVisibility(View.VISIBLE);
			groupViewHolder.itemGroupLayout
					.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							OnGroupExpandListener.onGroupExpand(groupPosition);
						}
					});
		}

		groupViewHolder.itemGroupText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onGroupClickListener.onGroupClick(groupPosition);
			}
		});

		groupViewHolder.itemArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				OnGroupExpandListener.onGroupExpand(groupPosition);
			}
		});

		// 判断isExpanded就可以控制按下还是关闭，同时更换图片,这里使用属性动画来控制旋转
		if (isExpanded) {
			// RotateAnimation rotateAnimation = new RotateAnimation(0, -180f,
			// RotateAnimation.RELATIVE_TO_SELF, 0.5f,
			// RotateAnimation.RELATIVE_TO_SELF, 0.5f);
			// rotateAnimation.setDuration(500);
			// rotateAnimation.setFillAfter(true);
			// groupViewHolder.itemArrow.setAnimation(rotateAnimation);
			groupViewHolder.itemArrow
					.setImageResource(R.drawable.ic_leftnav_up);
			// if (groupPosition == mGroupPosition) {
			// setItemChecked(mGroupPosition, mChildPosition);
			// }

			// 没有孩子项就不隐藏分割线
			if (childList.get(groupPosition).size() > 0) {
				groupViewHolder.itemDivider.setVisibility(View.INVISIBLE);
			} else {
				groupViewHolder.itemDivider.setVisibility(View.VISIBLE);
			}
		} else {
			// RotateAnimation rotateAnimation = new RotateAnimation(-180f, 0f,
			// RotateAnimation.RELATIVE_TO_SELF, 0.5f,
			// RotateAnimation.RELATIVE_TO_SELF, 0.5f);
			// rotateAnimation.setDuration(500);
			// rotateAnimation.setFillAfter(true);
			// groupViewHolder.itemArrow.setAnimation(rotateAnimation);
			groupViewHolder.itemArrow
					.setImageResource(R.drawable.ic_leftnav_down);
			groupViewHolder.itemDivider.setVisibility(View.VISIBLE);
		}

		return convertView;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final ChildViewHolder childViewHolder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.layout_expand_item,
					parent, false);
			childViewHolder = new ChildViewHolder(convertView);
			convertView.setTag(childViewHolder);
		} else {
			childViewHolder = (ChildViewHolder) convertView.getTag();
		}

		String content = childList.get(groupPosition).get(childPosition)
				.getTitle();

		// 设置内容
		childViewHolder.itemChildText.setText(content);

		// 设置文本点击事件
		childViewHolder.itemChildText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onChildClickListener.onChildItemClick(groupPosition,
						childPosition);
			}
		});

		if (childPosition == childList.get(groupPosition).size() - 1) {
			childViewHolder.itemDivider.setVisibility(View.VISIBLE);
		} else {
			childViewHolder.itemDivider.setVisibility(View.GONE);
		}

		// 设置子项被选中的状态


		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// 设置孩子项可选中
		return true;
	}

	private static class GroupViewHolder {
		RelativeLayout itemGroupLayout;
		ImageView itemGroupIcon;
		TextView itemGroupText;
		ImageView itemArrow;
		View itemDivider;

		public GroupViewHolder(View convertView) {
			itemGroupLayout = (RelativeLayout) convertView
					.findViewById(R.id.group_layout);
			itemGroupIcon = (ImageView) convertView
					.findViewById(R.id.iv_group_icon);
			itemGroupText = (TextView) convertView
					.findViewById(R.id.tv_group_text);
			itemArrow = (ImageView) convertView.findViewById(R.id.iv_expand);
			itemDivider = (View) convertView
					.findViewById(R.id.item_group_devider);

		}

	}

	private static class ChildViewHolder {
		TextView itemChildText;
		View itemDivider;

		public ChildViewHolder(View convertView) {
			itemChildText = (TextView) convertView
					.findViewById(R.id.tv_item_text);
			itemDivider = (View) convertView.findViewById(R.id.item_devider);

		}
	}

	public interface OnGroupExpandListener {
		void onGroupExpand(int position);
	}

	public interface OnGroupClickListener {
		void onGroupClick(int position);
	}

	public interface OnChildItemClickListener {
		void onChildItemClick(int groupPosition, int childPosition);
	}

	public boolean getExpandStateAtPosition(int groupPosition) {
		// 获得当前位置的展开状态
		expandStateAtPosition = maps.get(groupPosition).booleanValue();
		return expandStateAtPosition;
	}

	public void setExpandStateAtPosition(int groupPosition,
			boolean expandStateAtPosition) {
		this.expandStateAtPosition = expandStateAtPosition;
		maps.put(groupPosition, expandStateAtPosition);
	}
}
