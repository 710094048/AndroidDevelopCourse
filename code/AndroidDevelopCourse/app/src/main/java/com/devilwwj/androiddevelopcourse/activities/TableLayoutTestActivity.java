package com.devilwwj.androiddevelopcourse.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.devilwwj.androiddevelopcourse.R;

/**
 * 布局之TableLayout
 */
public class TableLayoutTestActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablelayout);

    }
}
