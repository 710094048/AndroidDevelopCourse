package com.devilwwj.androiddevelopcourse.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.devilwwj.androiddevelopcourse.R;
import com.devilwwj.androiddevelopcourse.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * A020-列表容器之ListView
 * @author devilwwj
 */
public class ListViewTestActivity extends ActionBarActivity implements OnItemClickListener{
    private ListView mListView;
    private ListViewAdapter mAdapter;
    private Context mContext;
    private List<User> userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);
        mContext = ListViewTestActivity.this;

        mListView = (ListView) this.findViewById(R.id.listView);


        /* 模拟数据 */
        userList = new ArrayList<User>();

        for (int i = 0; i < 20; i++) {
            User user = new User();
            user.setImageUrl("http://tp1.sinaimg.cn/2463074124/180/40037127109/1");
            user.setName("小巫" + i);
            user.setDescription("小巫不用说都应该知道是谁的啦！！！");
            userList.add(user);
        }

        mListView.setAdapter(new ListViewAdapter(this, userList));

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO: 点击列表跳转到其他页面

    }


    private class ListViewAdapter extends BaseAdapter {

        private List<User> users;

        public ListViewAdapter(Context context, List<User> users) {
            this.users = users;
        }

        @Override
        public int getCount() {
            return users != null ? users.size() : 0;
        }

        @Override
        public User getItem(int position) {
            return users.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;


            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_list_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.itemHead = (ImageView) convertView.findViewById(R.id.iv_head);
                viewHolder.itemName = (TextView) convertView.findViewById(R.id.tv_name);
                viewHolder.itemDesc = (TextView) convertView.findViewById(R.id.tv_desc);


                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            User user = getItem(position);
            if (user != null) {
                // 这里只是模拟，实际开发可能需要加载网络图片，可以使用ImageLoader这样的图片加载框架来异步加载图片
                viewHolder.itemHead.setImageResource(R.drawable.pic_6);
                viewHolder.itemName.setText(user.getName());
                viewHolder.itemDesc.setText(user.getDescription());

            }

            return convertView;
        }


        class ViewHolder {
            ImageView itemHead;
            TextView itemName;
            TextView itemDesc;
        }
    }
}
