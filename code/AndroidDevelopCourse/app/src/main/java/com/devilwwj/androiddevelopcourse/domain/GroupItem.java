package com.devilwwj.androiddevelopcourse.domain;

public class GroupItem {
	private int drawableId;
	private String text;
	private String groupId;
	public int getDrawableId() {
		return drawableId;
	}
	public void setDrawableId(int drawable) {
		this.drawableId = drawable;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	
	
	
}
