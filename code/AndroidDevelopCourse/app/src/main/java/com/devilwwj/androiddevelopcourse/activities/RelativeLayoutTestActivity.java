package com.devilwwj.androiddevelopcourse.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.devilwwj.androiddevelopcourse.R;


public class RelativeLayoutTestActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relativelayout);

    }
}
