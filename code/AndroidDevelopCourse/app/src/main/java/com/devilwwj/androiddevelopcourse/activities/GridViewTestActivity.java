package com.devilwwj.androiddevelopcourse.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.devilwwj.androiddevelopcourse.R;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * A021-列表容器之GridView
 *
 * @author devilwwj
 */
public class GridViewTestActivity extends ActionBarActivity implements OnItemClickListener {
    private GridView gridView;
    private Context mContext;
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gridview);
        mContext = GridViewTestActivity.this;

        imageLoader = ImageLoader.getInstance();

        gridView = (GridView) this.findViewById(R.id.gridView);


        gridView.setAdapter(new ImageAdapter(this));

    }

    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6, R.drawable.sample_7,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6, R.drawable.sample_7,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6, R.drawable.sample_7
    };


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO: 点击列表跳转到其他页面

    }


    private class ImageAdapter extends BaseAdapter {

        private Context mContext;

        public ImageAdapter(Context context) {
            this.mContext = context;
        }


        @Override
        public int getCount() {
            return mThumbIds.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder = null;


            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_grid_item, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.itemImg = (ImageView) convertView.findViewById(R.id.iv_head);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            // 这里只是模拟，实际开发可能需要加载网络图片，可以使用ImageLoader这样的图片加载框架来异步加载图片
            imageLoader.displayImage("drawable://" + mThumbIds[position], viewHolder.itemImg);


            return convertView;
        }


        class ViewHolder {
            ImageView itemImg;
        }
    }
}
