#AndroidDevelopCourse

>由于Google官方已经明确表示不再支持Eclipse，这就意味着后面的开发使用Android Studio来进行开发是少不了的了，为了能跟上Google的脚步，小巫打算使用Android Studio重新录制新的课程，从零开始教大家如何来学习Android核心知识，优化之前使用Eclipse的课程，以一种让大家更易接受的方式来讲解课程。前面已经发了一篇关于后续博文撰写计划[http://blog.csdn.net/wwj_748/article/details/46814935](http://blog.csdn.net/wwj_748/article/details/46814935)，各位读者可以先关注，博文会与视频教程内容同步，喜欢看文字的朋友可以看博文，喜欢看视频的朋友可以看视频，无论哪种方式都能够让各位入门。这次重录的内容，大部分内容还是入门知识，但比较有特色的是从实际开发角度来讲解，一些流行的框架使用也将会详细给大家介绍，学习完这套课程我相信开发一个商用app不再是难事，后续也会录制一些进阶课程，例如项目实战类型的，不过不会那么快出来，录制课程需要精力和时间，所以需要各位的支持，留个言点个赞表示一下呗。

#Android开发入门教程

##课程目录
1.  课程介绍
2.  开发工具介绍
3.  开发环境搭建
4.  Android Studio开发HelloWorld
5.  项目结构分析
6.  资源文件分析
    6-1 AndroidManifest.xml文件解析
    6-2 assets资源
    6-3 drawable资源
    6-4 layout资源
    6-5 menu资源
    6-6 anim资源
    6-7 values资源
7.  基础控件使用介绍
    7- 1TextView的使用
    7-2 Button的使用
    7-3 RadioButton的使用
    7-4 CheckBox的使用
    7-5 Switch的使用（Android 4.0以上使用）
    7-6 ToggleButton的使用
    7-7 ImageButton的使用
    7-8 ImageView的使用
    7-9 ProgressBar的使用
    7-10 SeekBar的使用
    7-11 RatingBar的使用
    7-12 Spinner的使用
    7-13 WebView的使用
    7-14 DataPicker&TimePicker 
8.  基础容器的使用介绍
    8-1 ListView的使用
    8-2 GridView的使用
    8-3 ExpandableListView的使用
    8-4 ScrollView的使用
9. 消息通知
    9-1 Toast
    9-2 Notification
9.  主流布局实现介绍
    9-1 LinearLayout(线性布局)
    9-2 RelativeLayout(相对布局)
    9-3 FrameLaout(帧布局)
    9-4 TableLayout（表格布局）
10. 五大组件介绍
    10-1    Activity(活动)
    10-2    Intent（意图）
    10-3    Service（服务）
    10-4    Broadcast Receiver（广播接受者）
    10-5    Content Provider（内容提供者）
11. 数据存储方式
    11-1 SQLite存储
    11-2 SharedPreference存储
    11-3 File存储
    11-4 Content Provider存储
    11-5 网络存储数据
13. 新特性控件
    13-1    Fragment
    13-2    ViewPager
    13-3    ActionBar
    13-4 ToolBar
    13-5 DrawerLayout
    13-6 RecyclerView
14. Android样式&主题
15. 动画&属性动画
    15-1    Animation
    15-2    Property Animator
16. 异步请求
    16-1 Handler+Thread
    16-2 AsyncTask
17. 实现App网络通信&数据解析
    17-1    搭建局域网服务端
    17-2    实现app联网
    17-3    xml数据解析
    17-4    json数据解析
18. 流行UI 设计效果
    18-1 Android-PullToRresh
    18-2 SlidingMenu
    18-3 ViewPagerIndicator
    18-4 PhotoView
    18-5 AndroidStaggeredGrid（瀑布流）
19. 流行网络框架&图片框架
    19-1 android-async-http
    19-2 Volley
    19-3 Android-Universal-Image-Loader
    19-4 picasso
    19-5 Fresco
20. 流行的数据存储框架
    20-1 ormLite
    20-2 GreenDao
21. 实战：搭建一个app框架
    
